package com.laithqotom.beamassessment.integration.providers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.laithqotom.beamassessment.dto.Hotel;
import com.laithqotom.beamassessment.dto.AvailableHotelsRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class CrazyHotelsProviderClient extends AbstractHotelsProviderClient<CrazyHotelsProviderClient.CrazyHotelsHotel> {

    private Logger LOG = LoggerFactory.getLogger(CrazyHotelsProviderClient.class);

    @Value("${crazyHotels.endpointUrl}")
    private String endpointUrl;

    @Override
    protected ParameterizedTypeReference<List<CrazyHotelsHotel>> getResponseType() {
        return new ParameterizedTypeReference<List<CrazyHotelsHotel>>() {
        };
    }

    @Override
    protected Logger getLogger() {
        return LOG;
    }

    @Override
    public MultiValueMap<String, String> buildQueryParams(AvailableHotelsRequest request) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>(4);
        params.add("city", request.getCity());
        params.add("from", formatDate(request.getFromDate()));
        params.add("To", formatDate(request.getToDate()));
        params.add("adultsCount", request.getNumberOfAdults().toString());
        return params;
    }

    private String formatDate(LocalDate date) {
        return LocalDateTime.of(date, LocalTime.NOON).atZone(ZoneId.systemDefault()).format(DateTimeFormatter.ISO_INSTANT);
    }

    @Override
    public String getProviderEndpointURL() {
        return endpointUrl;
    }

    @Override
    public String getProviderName() {
        return "CrazyHotels";
    }

    @Override
    public Hotel mapResponse(CrazyHotelsHotel ah) {
        Hotel availableHotel = new Hotel();
        availableHotel.setProvider(getProviderName());
        availableHotel.setRate(Float.valueOf(ah.getRate()));
        availableHotel.setHotelName(ah.getHotelName());
        availableHotel.setFarePerNight(ah.getPrice());
        availableHotel.setAmenities(ah.getAmenities());
        return availableHotel;
    }

    @JsonIgnoreProperties(ignoreUnknown=true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class CrazyHotelsHotel {
        String hotelName;
        Integer rate;
        Float price;
        Float discount;
        String[] amenities;

        public String getHotelName() {
            return hotelName;
        }

        public void setHotelName(String hotelName) {
            this.hotelName = hotelName;
        }

        public Integer getRate() {
            return rate;
        }

        public void setRate(String rate) {
            int r = 0;
            for (char star : rate.toCharArray())
                if (star == '*')
                    r++;
            this.rate = r;
        }

        public Float getPrice() {
            return discount == null ? price : price - discount;
        }

        public void setPrice(Float price) {
            this.price = price;
        }

        public Float getDiscount() {
            return discount;
        }

        public void setDiscount(Float discount) {
            this.discount = discount;
        }

        public String[] getAmenities() {
            return amenities;
        }

        public void setAmenities(String[] amenities) {
            this.amenities = amenities;
        }
    }
}
