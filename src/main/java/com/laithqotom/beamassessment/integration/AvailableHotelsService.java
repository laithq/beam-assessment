package com.laithqotom.beamassessment.integration;

import com.laithqotom.beamassessment.dto.Hotel;
import com.laithqotom.beamassessment.dto.AvailableHotelsRequest;
import com.laithqotom.beamassessment.integration.providers.AbstractHotelsProviderClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AvailableHotelsService {

    private final List<AbstractHotelsProviderClient> providers;

    @Autowired
    public AvailableHotelsService(List<AbstractHotelsProviderClient> providers) {
        this.providers = providers;
    }

    public Collection<Hotel> findHotelsFromAllProvidersSortedByRate(AvailableHotelsRequest request) {
        List<Hotel> hotels = new ArrayList<>();
        for (AbstractHotelsProviderClient provider : providers) {
            hotels.addAll(provider.findAvailableHotels(request));
        }
        hotels.sort(Comparator.comparing(Hotel::getRate).reversed());
        return hotels;
    }
}
