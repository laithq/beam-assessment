**Beam Team Assessment Project**
_by Laith Qotom_

* This is a Spring Boot application that runs with an embedded server, so no much configuration is required.
* I implemented dummy services that returns static data for /BestHotels & /CrazyHotels endpoints
* There's no persistence layer in this implementation
* Basic DTO validation is provided, this must be improved for production level
* Very basic testing is provided 