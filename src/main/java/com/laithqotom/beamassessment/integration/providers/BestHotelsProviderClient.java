package com.laithqotom.beamassessment.integration.providers;

import com.laithqotom.beamassessment.dto.Hotel;
import com.laithqotom.beamassessment.dto.AvailableHotelsRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;


@Service
public class BestHotelsProviderClient extends AbstractHotelsProviderClient<BestHotelsProviderClient.BestHotelsHotel> {

    private static final Logger LOG = LoggerFactory.getLogger(BestHotelsProviderClient.class);

    @Value("bestHotels.endpointUrl")
    private String endpointUrl;

    @Override
    public Hotel mapResponse(BestHotelsHotel ah) {
        Hotel availableHotel = new Hotel();
        availableHotel.setProvider(getProviderName());
        availableHotel.setAmenities(ah.getRoomAmenities().split(","));
        availableHotel.setFarePerNight(ah.getHotelFare());
        availableHotel.setHotelName(ah.getHotel());
        availableHotel.setRate(ah.getHotelRate());
        return availableHotel;
    }

    @Override
    protected ParameterizedTypeReference<List<BestHotelsHotel>> getResponseType() {
        return new ParameterizedTypeReference<List<BestHotelsHotel>>() {
        };
    }

    @Override
    protected Logger getLogger() {
        return LOG;
    }

    @Override
    public MultiValueMap<String, String> buildQueryParams(AvailableHotelsRequest request) {
        MultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>(5);
        requestParams.add("city", request.getCity());
        requestParams.add("from", formatDate(request.getFromDate()));
        requestParams.add("to", formatDate(request.getToDate()));
        requestParams.add("numberOfAdults", request.getNumberOfAdults().toString());
        return requestParams;
    }

    @Override
    public String getProviderName() {
        return endpointUrl;
    }

    @Override
    public String getProviderEndpointURL() {
        return "http://localhost:8080/BestHotels";
    }

    private static String formatDate(LocalDate date) {
        return date.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

    public static class BestHotelsHotel {
        String hotel;
        Float hotelRate;
        Float hotelFare;
        String roomAmenities;

        public String getHotel() {
            return hotel;
        }

        public void setHotel(String hotel) {
            this.hotel = hotel;
        }

        public Float getHotelRate() {
            return hotelRate;
        }

        public void setHotelRate(Float hotelRate) {
            this.hotelRate = hotelRate;
        }

        public Float getHotelFare() {
            return hotelFare;
        }

        public void setHotelFare(Float hotelFare) {
            this.hotelFare = hotelFare;
        }

        public String getRoomAmenities() {
            return roomAmenities;
        }

        public void setRoomAmenities(String roomAmenities) {
            this.roomAmenities = roomAmenities;
        }
    }
}
