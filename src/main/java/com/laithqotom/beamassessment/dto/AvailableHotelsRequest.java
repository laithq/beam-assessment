package com.laithqotom.beamassessment.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class AvailableHotelsRequest {

    @NotNull
    private LocalDate fromDate;

    @NotNull
    private LocalDate toDate;

    @NotNull
    private String city; //IATA city code TODO: implement custom validator for this field

    @Min(0)
    private Integer numberOfAdults;

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = LocalDate.parse(fromDate, DateTimeFormatter.ISO_LOCAL_DATE);
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = LocalDate.parse(toDate, DateTimeFormatter.ISO_LOCAL_DATE);
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getNumberOfAdults() {
        return numberOfAdults;
    }

    public void setNumberOfAdults(Integer numberOfAdults) {
        this.numberOfAdults = numberOfAdults;
    }

    @Override
    public String toString() {
        return "AvailableHotelsRequest{" +
                "fromDate=" + fromDate +
                ", toDate=" + toDate +
                ", city='" + city + '\'' +
                ", numberOfAdults=" + numberOfAdults +
                '}';
    }
}
