package com.laithqotom.beamassessment.controller;

import com.laithqotom.beamassessment.dto.Hotel;
import com.laithqotom.beamassessment.integration.AvailableHotelsService;
import com.laithqotom.beamassessment.util.TestUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
public class AvailableHotelControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AvailableHotelsService service;

    @Test
    public void findHotelsFromAllProviders_validRequestShouldPass() throws Exception {
        mockMvc.perform(get("/findHotels")
                .param("city", "amm")
                .param("fromDate", LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE))
                .param("toDate", LocalDate.now().plusDays(3).format(DateTimeFormatter.ISO_LOCAL_DATE))
                .param("numberOfAdults", "3"))
                .andExpect(status().isOk());
    }

    @Test
    public void hotelsFromAllProviders_invalidRequestShouldFail() throws Exception {
        mockMvc.perform(get("/findHotels")
                .param("city", "amm")
                .param("fromDate", LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE))
                .param("toDate", LocalDate.now().plusDays(3).format(DateTimeFormatter.ISO_LOCAL_DATE))
                .param("numberOfAdults", "-1"))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void hotelsFromAllProviders_invalidRequestShouldFail2() throws Exception {
        mockMvc.perform(get("/findHotels")
                .param("city", "amm"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void hotelsFromAllProviders_mustBeSortedByRate() {
        AvailableHotelController controller = new AvailableHotelController(service);
        ResponseEntity<Collection<Hotel>> hotelsFromAllProviders = controller.findHotelsFromAllProviders(TestUtil.getDummyRequestTest());
        assertNotNull(hotelsFromAllProviders);
        assertNotNull(hotelsFromAllProviders.getBody());
        assertNotEquals(hotelsFromAllProviders.getBody().size(), 0);
        List<Hotel> response = new ArrayList<>(hotelsFromAllProviders.getBody());
        List<Hotel> sorted = new ArrayList<>(hotelsFromAllProviders.getBody());
        sorted.sort(Comparator.comparing(Hotel::getRate).reversed());
        assertEquals(response, sorted);
    }
}