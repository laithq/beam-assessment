package com.laithqotom.beamassessment.integration.providers;

import com.laithqotom.beamassessment.dto.AvailableHotelsRequest;
import com.laithqotom.beamassessment.dto.Hotel;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Acts as a strategy for all hotels providers, this class is responsible of making, managing, validating
 * REST calls to provider's endpoints
 */
public abstract class AbstractHotelsProviderClient<T> {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    Validator validator;

    /**
     * Finds available hotels from this provider based on provided request
     * @param request the available hotels request
     * @return list of hotels if external API returned 200 response
     *          empty list otherwise
     * @throws IllegalArgumentException when there are validation errors
     */
    public List<? extends Hotel> findAvailableHotels(AvailableHotelsRequest request) {
        URI url = UriComponentsBuilder
                .fromHttpUrl(getProviderEndpointURL())
                .queryParams(buildQueryParams(request))
                .build()
                .toUri();
        ParameterizedTypeReference<List<T>> responseType = getResponseType();
        ResponseEntity<List<T>> response = restTemplate.exchange(url,
                HttpMethod.GET,
                null,
                responseType);
        if (response.getStatusCode() != HttpStatus.OK)
            getLogger().error("Bad response from BestHotels:" + response.toString());
        else if (response.getBody() != null) {
            List<Hotel> hotels = response.getBody().stream()
                    .map(this::mapResponse)
                    .collect(Collectors.toList());
            hotels.forEach(v -> {
                Set<ConstraintViolation<Hotel>> violations = validator.validate(v);
                if (!violations.isEmpty())
                    throw new IllegalArgumentException("Illegal value(s) returned from external API:" +
                            violations.stream().map(ConstraintViolation::getMessage).collect(Collectors.toList()).toString());
            });
            return hotels;
        }
        return Collections.emptyList();
    }

    /**
     * Maps the provider's internal POJO to the standard DTO: {@link Hotel}
     * @param ah the internal hotel POJO that represents the external API response
     * @return hotel POJO standard DTO format for BEAM APP: {@link Hotel}
     */
    public abstract Hotel mapResponse(T ah);

    protected abstract ParameterizedTypeReference<List<T>> getResponseType();

    protected abstract Logger getLogger();

    public abstract MultiValueMap<String, String> buildQueryParams(AvailableHotelsRequest request);

    public abstract String getProviderEndpointURL();

    public abstract String getProviderName();

}
