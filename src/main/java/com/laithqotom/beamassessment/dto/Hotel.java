package com.laithqotom.beamassessment.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class Hotel {

    @NotNull
    private String provider; //the name of the provider where this hotel were retrieved

    @NotNull
    private String hotelName;

    @Min(0)
    private Float farePerNight;

    @Nullable
    private String[] amenities;

    @Min(1)
    @Max(5)
    @JsonIgnore
    private Float rate; //hotel rating from 1 to 5

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public Float getFarePerNight() {
        return farePerNight;
    }

    public void setFarePerNight(Float farePerNight) {
        this.farePerNight = farePerNight;
    }

    public String[] getAmenities() {
        return amenities;
    }

    public void setAmenities(String[] amenities) {
        this.amenities = amenities;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }
}
