package com.laithqotom.beamassessment.controller;

import com.laithqotom.beamassessment.dto.AvailableHotelsRequest;
import com.laithqotom.beamassessment.dto.Hotel;
import com.laithqotom.beamassessment.integration.AvailableHotelsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collection;

@RestController
/*
  A controller that holds the main functionality of this application
  it servers /findHotels requests
 */
public class AvailableHotelController {

    private AvailableHotelsService availableHotelsService;

    @Autowired
    public AvailableHotelController(AvailableHotelsService availableHotelsService) {
        this.availableHotelsService = availableHotelsService;
    }

    @RequestMapping(value = "/findHotels", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Collection<Hotel>> findHotelsFromAllProviders(@Valid AvailableHotelsRequest request) {
        return new ResponseEntity<>(availableHotelsService.findHotelsFromAllProvidersSortedByRate(request), HttpStatus.OK);
    }

}
