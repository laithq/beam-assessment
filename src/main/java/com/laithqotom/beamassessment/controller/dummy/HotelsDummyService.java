package com.laithqotom.beamassessment.controller.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
/*
  Dummy service that returns no static response for /BestHotels & /CrazyHotels endpoints
 */
public class HotelsDummyService {

    private static final Logger LOG = LoggerFactory.getLogger(HotelsDummyService.class);

    @GetMapping(value = "/BestHotels", produces = "application/json")
    public String dummyBestHotelsRequest(HttpServletRequest request) {
        LOG.debug(request.getParameterMap().toString());
        return "[\n" +
                "  {\n" +
                "    \"hotel\": \"dummy HOTEL\",\n" +
                "    \"hotelRate\": \"4.9\",\n" +
                "    \"hotelFare\": \"72.22\",\n" +
                "    \"roomAmenities\": \"shower,desk,other\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"hotel\": \"HOTELSINTON\",\n" +
                "    \"hotelRate\": \"5\",\n" +
                "    \"hotelFare\": \"92\",\n" +
                "    \"roomAmenities\": \"private pool,shower,desk,other\"\n" +
                "  }\n" +
                "]";
    }

    @GetMapping(value = "/CrazyHotels", produces = "application/json")
    public String dummyCrazyHotelsService(HttpServletRequest request) {
        LOG.debug(request.getParameterMap().toString());
        return "[\n" +
                "  {\n" +
                "    \"hotelName\": \"WHAT A CRAZY HOTEL\",\n" +
                "    \"rate\": \"**\",\n" +
                "    \"price\": \"99.9\",\n" +
                "    \"discount\": \"12\",\n" +
                "    \"amenities\" : [\n" +
                "      \"pool\",\n" +
                "      \"shower\",\n" +
                "      \"desk\"\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"hotelName\": \"WHAT A CRAZY HOTEL WITHOUT DISCOUNT\",\n" +
                "    \"rate\": \"****\",\n" +
                "    \"price\": \"99.9\",\n" +
                "    \"amenities\" : [\n" +
                "      \"pool\",\n" +
                "      \"shower\",\n" +
                "      \"desk\"\n" +
                "    ]\n" +
                "  }\n" +
                "\n" +
                "]";
    }

}
