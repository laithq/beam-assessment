package com.laithqotom.beamassessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@RestController
public class BeamAssessmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeamAssessmentApplication.class, args);
	}

	@RequestMapping(path = "/testRest", method = RequestMethod.GET)
	public Map<String, Object> testRest(@RequestParam String param) {
		HashMap<String, Object> model = new HashMap<>();
		model.put("sent", param);
		model.put("another", "plala");
		return model;
	}

}

