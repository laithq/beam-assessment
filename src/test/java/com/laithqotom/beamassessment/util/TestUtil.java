package com.laithqotom.beamassessment.util;

import com.laithqotom.beamassessment.dto.AvailableHotelsRequest;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class TestUtil {

    public static AvailableHotelsRequest getDummyRequestTest() {
        AvailableHotelsRequest request = new AvailableHotelsRequest();
        request.setCity("city");
        request.setFromDate(LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE));
        request.setToDate(LocalDate.now().plusDays(4).format(DateTimeFormatter.ISO_LOCAL_DATE));
        request.setNumberOfAdults(3);
        return request;
    }

}
